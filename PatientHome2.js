(function(){
    var newPatient = new Vue({
        el: "#patientList",
        data: {
            patients:[
                {
                    image:'profileIcon.png',
                    name: '',
                    surname: '',
                    gender: '',
                    age: ''
                }
            ]
        },
        methods: {
            addNewPatient(name, surname, gender, age, image){
                this.patients.push({
                    image: image,
                    name: name,
                    surname: surname,
                    gender: gender,
                    age: age
                })
            },
            gereratePatients(){
                for(let i = 1; i < 32; i++){
                    var image;
                    if(i%2 == 0){
                        sex = "Female";
                        this.image='profileIcon2.png'
                    }else{
                        sex = "Male";
                        this.image='profileIcon.png'
                    }
                    this.addNewPatient("dummy"+i, "dummy"+i, sex, 50+i, this.image);
                }
            }
        },
        
    })
	
    newPatient.gereratePatients();
    var s1=document.getElementById("p1");
    s1.innerHTML = "";
    s1.setAttribute("class", "aaaa");
    
    
}());
function LoadPatientsPage(PatientsData){
    $('body').empty();
    $('body').append(
    



'<div class="container-fluid"id="MainPage">'
+            '<div class="row justify-content-center my-5">'
+               '<div class="col-2 topBtn">'
+                   '<button onclick="GoBackFromPatient()" class="btn topBtn">Back</button>'
+                '</div>'
+                '<div class="col-8 header">'
+                    '<header>Patients</header>'
+                '</div>'
+                '<div class="col-2 topBtn">'
+                    '<button onclick="GoToHome()" class="btn topBtn">Home</button>'
+                '</div>'
+            '</div>'
+			 '<div class="row justify-content-center">'
+				 '<ul id="patientList">'
+                	'<button onclick="LoadCalendar()" id="p0" class="patient">'
+                		'<form id="info">'
+                			'<li class="col-4 patient" v-for="(patient, index) in patients" id="column">'
+                                		 '<label class="lab" for="nameField">Name:</label>'
+                                		 '<img v-bind:src="patients[index].image" class="profileIcon"/>'
+                                        '<input type="text" class="field" id="nameField" v-model="patient.name" readonly>'
+                                        '<br>'
+                                        '<br>'
+                                        '<label class="lab" for="surnameField">Surname:</label>'
+                                        '<input type="text" class="field" id="surnameField" v-model="patient.surname" readonly>'
+                                        '<br>'
+                                        '<br>'
+                                        '<label class="lab" for="genderField">Gender:</label>'
+                                        '<input type="text" class="field" id="genderField" v-model="patient.gender" readonly>'
+                                        '<br>'
+                                        '<br>'
+                                        '<label class="lab" for="ageField">Age:</label>'
+                                        '<input type="text" class="field" id="ageField" v-model="patient.age" readonly>'
+				           '</li>'  
+                       '</form>'         
+                   '</button>'                    
+                '</ul>'                   
+            '</div>'
+        '</div>'

);









}

function LoadPatients(json){
    function callback(response){
        LoadPatientsPage(response);//Load Data of doctor.
    }
    callback(); //use this in order to load the next page automatically,if sign in data exchange works then remove.
    var data = {"username" : "DocID" };   //add name from json argument.
    ajax_request(POST,'Where to send - Add.',data,callback);
}

