(function(){
    var newPatient = new Vue({
        el: "#p1",
        data: {
            patient:
                {
                    image:'',
                    name: '',
                    surname: '',
                    gender: '',
                    age: ''
                }
            
        },
        methods: {
            addNewPatient(name, surname, gender, age, image){
                Vue.set(newPatient.patient, 'name', name)
                Vue.set(newPatient.patient, 'surname', surname)
                Vue.set(newPatient.patient, 'gender', gender)
                Vue.set(newPatient.patient, 'age', age)
                Vue.set(newPatient.patient, 'image', image)
            },
        },
        
        
    })
    newPatient.addNewPatient("dummy1", "dummy1", "Male", "51", 'profileIcon.png');
}());



function LoadCalendar(){
    $('body').empty();
    $('body').append(
    '<div class="containter-fluid">'
+	'<div class="row justify-content-center mt-2">'
+		'<div class="col-2">'
+		'<button onclick="GoBackFromCalendar()"class="topBtn btn backBtn">Back</button>'
+		'</div>'
+		'<div class="col-8"></div>'
+		'<div class="col-2">'
+			'<button onclick="GoToHome()"class="topBtn btn homeBtn">Home</button>'
+		'</div>'
+	'</div>'
+			'<div class="row justify-content-center">'
+			'<div class="col-2">'
+				'<label class="lab lab1" for="from">Start Date</label>'
+				'<br>'
+				'<input type="date" id="from" name="from">'
+				'<label class="lab lab2" for="to">End Date</label>'
+				'<br>'
+				'<input type="date" id="to" name="to">'
+			'</div>'
+	'<div class="col-10">'
+	'<div id="bar1chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+	'<div id="bar2chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+	'<div id="bar3chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+	'<div id="bar4chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+	'<div id="bar5chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-5">'
+		'<div id="pie1chart_div"></div>'
+	'</div>'
+	'<div class="col-5">'
+		'<div id="pie2chart_div"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+		'<div id="columnchart_material"></div>'
+	'</div>'
+	'</div>'
+	'<div class="row justify-content-center">'
+	'<div class="col-2"></div>'
+	'<div class="col-10">'
+		'<div id="chart_lines"></div>'
+	'</div>'
+	'</div>'
+	'</div>'

);
}